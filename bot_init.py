from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram import ParseMode
from twitch import TwitchHelix
import re
import random
import os
from io import StringIO

PLAYERS = ['Romeruk', 'toperharlimadhad', 'Vova Solovets']
WHOM_TO_PICK_WORDS = ["кого", "пікати"]
ADVICE_TEXT = """Корисні ссилки:
https://www.dotabuff.com/heroes/winning - показує мету
https://www.youtube.com/watch?v=26gb8QOzYpA - показує осмислений аналіз патчу
https://www.youtube.com/watch?v=QUnMjAeJoNs - те саме, тільки від НСа
https://www.youtube.com/watch?v=ac6qcyL_-EI - як контрити МЕТУ
https://www.youtube.com/watch?v=SLD72jFottQ - Тут треба дивитись всі випуски
https://www.youtube.com/watch?v=jrlnKVrc-Ec - САЦИК від світу доти
Ссилки будуть оновлюватись, так що не нервуйте в разі чого(головне - не забудьте нагадати)
"""
TELEGRAM_TOKEN = os.environ.get('TELEGRAM_API_TOKEN')
TWITCH_TOKEN = os.environ.get('TWITCH_API_TOKEN')
TWITCH_CLIENT = TwitchHelix(client_id=TWITCH_TOKEN)
SUCCESS_MESSAGE = "<b>%(streamer_name)s</b> is now live!\nWatch him online at %(stream_url)s"
FAIL_MESSAGE = "Unfortunately, <b>%(streamer_name)s</b> is now offline"


updater = Updater(token=TELEGRAM_TOKEN)
dispatcher = updater.dispatcher


def roll(seed=None, min=0, max=99):
    random.seed(seed)
    num = random.randint(min, max)
    return num

def rolldefault(bot,update):
    num = roll()
    bot.send_message(chat_id=update.message.chat_id, text=str(num))

def group_roll(bot, update):
    rolls = {player:roll() for player in PLAYERS}
    results = sorted(rolls.keys(), key=lambda x:rolls[x], reverse=True)
    text = "\n".join(["<b>%s</b> - %s" % (key, rolls[key]) for key in results])
    bot.send_message(chat_id=update.message.chat_id, text=text, 
                     parse_mode=ParseMode.HTML)

def set_random_positions(bot, update):
    try:
        positions = ['Carry','Mid','Hard lane','pos 4','pos 5']
        player_positions = {}
        for player in PLAYERS:
            num = random.choice(positions)
            player_positions.update({player: num})
            positions.remove(num)
        pos_text = "\n".join(["<b>%s</b> - %s" %(key, player_positions[key]) for key in player_positions])
        bot.send_message(chat_id=update.message.chat_id, text=pos_text, 
                         parse_mode=ParseMode.HTML)
    except Exception as error:
        print(error)

def pick_heroes_advice(bot, update):
    message_text = update.message.text
    if all([re.search(word, message_text) for word in WHOM_TO_PICK_WORDS]):
        bot.send_message(chat_id=update.message.chat_id, text=ADVICE_TEXT)

def check_versuta(bot, update):
    try:
        if TWITCH_CLIENT.get_streams_metadata(user_logins=['versuta']):
            bot.send_message(chat_id=update.message.chat_id, text=SUCCESS_MESSAGE % {'streamer_name': 'TOP 1 WORLD PLAYER', 
                                                                                     'stream_url': 'twitch.tv/versuta'}, 
                             parse_mode=ParseMode.HTML)
        else:
            bot.send_message(chat_id=update.message.chat_id,text=FAIL_MESSAGE %{'streamer_name': 'TOP 1 WORLD PLAYER'}, 
                             parse_mode=ParseMode.HTML)
    except Exception as error:
        bot.send_message(chat_id=update.message.chat_id, text="The error is %s" % error)

def check_stray(bot, update):
    try:
        if TWITCH_CLIENT.get_streams_metadata(user_logins=['stray228']):
            bot.send_message(chat_id=update.message.chat_id, text=SUCCESS_MESSAGE % {'streamer_name': 'Krisa', 
                                                                                     'stream_url': 'twitch.tv/stray228'}, 
                             parse_mode=ParseMode.HTML)
        else:
            bot.send_message(chat_id=update.message.chat_id,text=FAIL_MESSAGE %{'streamer_name': 'Krisa'}, 
                             parse_mode=ParseMode.HTML)
    except Exception as error:
        bot.send_message(chat_id=update.message.chat_id, text="The error is %s" % error)

def agitation(bot, update):
    bot.send_message(chat_id=update.message.chat_id, 
                     text='Порошенко наш президент!!! <strong>ГОЛОСУЙТЕ ЗА НЬОГО І ОТРИМАЙТЕ ТИСЯЧУ!!!!!111</strong>', 
                     parse_mode=ParseMode.HTML)

def send_students_list(bot, update):
    students_list = sorted(open('student_list.txt', 'r').read().strip().split('\n'))
    FILE_NAME = 'students_list.txt'
    output_file = open(FILE_NAME, 'w')
    output_str = '#\tПрізвище\r\n'
    output_str += '\r\n'.join(['%i\t%s' % (i+1, students_list[i]) for i in range(len(students_list))])
    print(output_str, file=output_file)
    output_file.close()
    bot.send_document(chat_id=update.message.chat_id, document=open(FILE_NAME, 'rb'))
    os.remove(FILE_NAME)
    
    

functions = {
    rolldefault: "rolldef", 
    group_roll: "group_roll", 
    set_random_positions:"randpos",
    check_versuta: "checkversuta",
    check_stray: "checkstray",
    agitation: 'порох',
    send_students_list: 'student_list'
}
for function in functions:
    roll_handler = CommandHandler(functions[function], function)
    dispatcher.add_handler(roll_handler)
message_handler_functions = [pick_heroes_advice]
for handler_function in message_handler_functions:
    message_handler = MessageHandler(Filters.text, pick_heroes_advice)
    dispatcher.add_handler(message_handler)
updater.start_polling()
